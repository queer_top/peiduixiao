package org.nathan.tool;

/**
 * 检测确认注册
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class CheckRegisTool {

	public static void checkRegis(String acc, String pwd, String reset, String name) {

		// 构建sql语句
		String registerSql = "select * from t_admin where acc = '" + acc + "'";
		String insertSql = "insert into t_admin (acc ,pwd ,name) values ('" + acc + "','" + pwd + "','" + name + "')";

		// 数据库连接
		Tool.connectingMysql();

		// 初始化conn
		Connection conn = null;

		// 获取连接
		try {

			// 返回一个链接，用connection接收 sql 8
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jf231207db", "jf231207", "123456");

			// 创建会话对象 并接收
			Statement st = conn.createStatement();

			// 使用会话对象，执行sql语句执行查询语句并返回查询结果集 并接收
			ResultSet rs = st.executeQuery(registerSql);

			// 如果有，则账号存在
			if (rs.next()) {
				JOptionPane.showMessageDialog(null, "账号已存在！");
			} else {
				st.executeUpdate(insertSql);
				JOptionPane.showMessageDialog(null, "注册成功！");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// 数据库连接关闭
		Tool.closeMysql(conn, null, null);
	}
}
