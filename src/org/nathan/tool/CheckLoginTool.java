package org.nathan.tool;

/**
 * 检测用户登入
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import org.nathan.model.Database;

public class CheckLoginTool {

	public static boolean checkLogin(String acc, String pwd) {

		// 构建sql语句
		String loginSql = "select * from t_admin where acc = '" + acc + "' and pwd = '" + pwd + "'";
		// 1'or'1'='1

		// 连接数据库
		Tool.connectingMysql();

		// 初始化conn
		Connection conn = null;
		// 获取连接
		try {

			// 返回一个链接，用connection接收
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jf231207db", "jf231207", "123456");

			// 创建会话对象 并接收
			Statement st = conn.createStatement();

			// 使用会话对象，执行sql语句,执行查询语句并返回查询结果集 并接收
			ResultSet rs = st.executeQuery(loginSql);

			// 如果有则取出
			if (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");

				// 获取玩家姓名
				Database.name = name;
				Database.id = id;
				JOptionPane.showMessageDialog(null, "登入成功！欢迎玩家【" + Database.name + "】！");
				return true;
			} else {
				JOptionPane.showMessageDialog(null, "账号密码有误，请重新输入！");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// 结束数据库连接
		Tool.closeMysql(conn, null, null);
		return false;
	}

}
