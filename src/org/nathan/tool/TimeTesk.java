package org.nathan.tool;

/**
 * 游戏计时器
 */

import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.nathan.model.Database;
import org.nathan.view.GameFrame;

public class TimeTesk extends TimerTask {

	private GameFrame gameFrame;

	public TimeTesk(GameFrame gameFrame) {
		super();
		this.gameFrame = gameFrame;
	}

	@Override
	public void run() {
		if (Database.status != 2) {
			return;
		}

		if (gameFrame.getGamePanel().getFrontList().size() > 0) {
			Database.time++;
		} else {
			JOptionPane.showMessageDialog(gameFrame, "恭喜你，已全部消除！用时：【" + Database.time + "】");
			InsertDataTool.insertData(Database.id, Database.time, Database.state);
			this.cancel();
		}
	}
}
