package org.nathan.tool;

/**
 * 把数据插入到数据库的方法
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class InsertDataTool {

	public static void insertData(int id, int time, int state) {

		// 构建sql语句

		String insertSql = "insert into t_time (id ,time ,difficulty) values ('" + id + "','" + time + "','" + state
				+ "')";

		// 数据库连接
		Tool.connectingMysql();

		// 初始化conn
		Connection conn = null;

		// 获取连接
		try {
			// 返回一个链接，用connection接收 sql 8
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jf231207db", "jf231207", "123456");

			// 创建会话对象 并接收
			Statement st = conn.createStatement();

			// 使用会话对象，执行sql语句执行查询语句并返回查询结果集 并接收
			int result = st.executeUpdate(insertSql);

			// 添加数据
			if (result > 0) {
				JOptionPane.showMessageDialog(null, "数据载入成功！");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// 数据库连接关闭
		Tool.closeMysql(conn, null, null);
	}
}
