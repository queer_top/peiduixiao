package org.nathan.tool;

/*
 * 音频.play()  播放一次
 * 音频.stop()  停止播放
 * 音频.loop()  循环播放
 */

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;

public class MusicTool {

	// 文件对象
	private File bgmFile;
	// 音频对象
	private AudioClip bgmAc;

	public MusicTool() {

		// 初始化文件与音频
		bgmFile = new File("music/Lianyi.wav");

		try {
			bgmAc = Applet.newAudioClip(bgmFile.toURI().toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	// 获取背景音乐音频
	public AudioClip getBgmAc() {
		return bgmAc;
	}

}
