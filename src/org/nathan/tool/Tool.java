package org.nathan.tool;

/**
 * 工具
 */

import java.awt.Image;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import javax.swing.ImageIcon;
import org.nathan.model.Database;
import org.nathan.model.Hero;

public class Tool {

	// Mysql连接
	public static void connectingMysql() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("依赖加载失败...");
		}
	}

	// Mysql关闭
	public static void closeMysql(Connection conn, Statement st, ResultSet rs) {
		try {
			if (Objects.nonNull(rs)) {
				rs.close();
			}
			if (Objects.nonNull(st)) {
				st.close();
			}
			if (Objects.nonNull(conn)) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// 初始化集合的方法
	public static void initheros(ArrayList<Hero> heros, int lattice) {
		// 清空集合
		heros.clear();
		// 随机不重复
		ArrayList<Integer> numRan = new ArrayList<Integer>();
		for (int i = 1; i <= 99; i++) {
			numRan.add(i);
		}
		Collections.shuffle(numRan);
		ArrayList<Integer> integers = new ArrayList<Integer>();
		for (int i = 0; i < 5; i++) {
			integers.add(numRan.get(i));
		}

		int index;
		for (int j = 0; j < (lattice * lattice) / 2; j++) {
			index = integers.get(j % 5);
			Image images = new ImageIcon("image/(" + (index + 1) + ").png").getImage();
			Hero hero = new Hero(index, images);
			Hero he = new Hero(index, images);
			heros.add(hero);
			heros.add(he);
		}
		Collections.shuffle(heros);
	}

	// 更新坐标的方法
	public static void updateCoordinates(ArrayList<Hero> heros, int lattice) {

		int count = 0;
		int line = 0;
		for (int i = 0; i < heros.size(); i++) {
			Hero s = heros.get(i);
			s.setStartX(Database.X_GAP + (576 / lattice) * count);
			s.setStartY(Database.Y_GAP + 60 + (576 / lattice) * line);
			count++;
			if (count % lattice == 0) {
				line++;
				count = 0;
			}
		}
	}

	// 鼠标点击
	public static Hero mouseCleck(int x, int y, ArrayList<Hero> heros, int lattice) {
		for (int i = 0; i < heros.size(); i++) {
			Hero hero = heros.get(i);
			if (x > hero.getStartX() && y > hero.getStartY() && x < hero.getStartX() + 576 / lattice
					&& y < hero.getStartY() + 576 / lattice) {
				return hero;
			}
		}
		return null;
	}
}
