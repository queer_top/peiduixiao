package org.nathan.tool;

/**
 * 展示排行榜
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectTool {

	public static ResultSet selectData(int state) {

		// 构建查询sql语句
		String selectSql = "select a.acc 账号, a.name 昵称, min(b.time)用时" 
				+ " from t_admin a "
				+ "inner join t_time b "
				+ "on a.id = b.id " 
				+ "where difficulty = " + state + " " 
				+ "group by a.id " 
				+ "order by min(b.time)";

		Tool.connectingMysql();
		Connection conn = null;

		// 获取连接
		try {
			// 返回一个链接，用connection接收 sql 8
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jf231207db", "jf231207", "123456");

			// 创建会话对象 并接收
			Statement st = conn.createStatement();

			// 使用会话对象，执行sql语句执行查询语句并返回查询结果集 并接收
			ResultSet result = st.executeQuery(selectSql);
			return result;

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// 数据库连接关闭
		Tool.closeMysql(conn, null, null);
		return null;
	}
}
