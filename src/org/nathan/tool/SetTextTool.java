package org.nathan.tool;

/**
 * 文本域内容显示的方法
 */

import java.sql.ResultSet;
import java.sql.SQLException;

public class SetTextTool {

	public static String setText(int state) {

		String str = "======================================\r\n" 
				+ "序号\t账号\t昵称\t用时\r\n"
				+ "======================================\r\n";

		ResultSet rs = SelectTool.selectData(state);
		
		// 序号
		int index = 0;
		try {
			while (rs.next()) {
				String acc = rs.getString("账号");
				String name = rs.getString("昵称");
				String time = rs.getString("用时");

				index++;
				str += index + "\t" + acc + "\t" + name + "\t" + time + "\r\n";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}
}
