package org.nathan.app;

/**
 * 主函数
 */

import org.nathan.controller.Application;

public class App {

	public static void main(String[] args) {
		Application.run();
	}
}
