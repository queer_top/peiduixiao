package org.nathan.view;

/**
 * 中间容器
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
import org.nathan.model.Database;
import org.nathan.model.Hero;

public class GamePanel extends JPanel {

	private static final long serialVersionUID = -1844183908188677183L;
	private Color col = new Color(137, 102, 208);
	private Color color = new Color(255, 175, 175);
	private Font font = new Font("", Font.BOLD, 20);

	// 后台集合
	private ArrayList<Hero> herosList = new ArrayList<Hero>();
	// 前台集合
	private ArrayList<Hero> frontList = new ArrayList<Hero>();
	// 点击比较集合
	private ArrayList<Hero> comparisonList = new ArrayList<Hero>();

	public GamePanel() {
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// 实心矩形
		g.setColor(color);
		g.fillRect(Database.X_GAP, Database.Y_GAP, 576, 50);

		// 内容：玩家
		g.setColor(col);
		g.setFont(font);
		g.drawString("玩家：【" + Database.name + "】", 30, 50);

		// 内容：难度
		g.setColor(col);
		g.setFont(font);
		g.drawString("难度：【" + Database.state + "】", 200, 50);

		// 计时器
		g.setColor(col);
		g.setFont(font);
		g.drawString("计时：" + Database.time, 340, 50);

		// 游戏区
		g.setColor(color);
		g.fillRect(Database.X_GAP, 80, 576, 576);

		// 绘制网格
		for (int i = 0; i <= Database.lattice; i++) {
			g.setColor(col);
			g.drawLine(Database.X_GAP, 80 + 576 / Database.lattice * i, 596, 80 + 576 / Database.lattice * i);
			g.drawLine(Database.Y_GAP + 576 / Database.lattice * i, 80, Database.X_GAP + 576 / Database.lattice * i,
					656);
		}

		// 绘制前台集合
		for (int i = 0; i < frontList.size(); i++) {
			frontList.get(i).showMe(g, Database.lattice);

		}

		repaint();

	}

	public ArrayList<Hero> getHerosList() {
		return herosList;
	}

	public void setHerosList(ArrayList<Hero> herosList) {
		this.herosList = herosList;
	}

	public ArrayList<Hero> getFrontList() {
		return frontList;
	}

	public void setFrontList(ArrayList<Hero> frontList) {
		this.frontList = frontList;
	}

	public ArrayList<Hero> getComparisonList() {
		return comparisonList;
	}

	public void setComparisonList(ArrayList<Hero> comparisonList) {
		this.comparisonList = comparisonList;
	}

}
