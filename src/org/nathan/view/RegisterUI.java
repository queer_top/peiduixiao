package org.nathan.view;

/**
 * 注册界面
 */

import java.awt.Font;
import java.awt.HeadlessException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RegisterUI extends JFrame {

	private static final long serialVersionUID = -8371333030338750882L;
	private LoginUI login;
	private RegisterUI register;

	public RegisterUI(LoginUI login, RegisterUI register) throws HeadlessException {
		super();
		this.login = login;
		this.register = register;
	}

	// 创建按钮
	private JButton resetButton = new JButton("重置");
	private JButton confirmButton = new JButton("确认注册");

	// 创建标签
	private JLabel welcomeLabel = new JLabel("用户注册");
	private JLabel enterNameLabel = new JLabel("输入名字");
	private JLabel enterAccountLabel = new JLabel("输入账号");
	private JLabel enterPasswordLabel = new JLabel("输入密码");
	private JLabel resetPasswordLabel = new JLabel("重复密码");

	// 创建文本框
	private JTextField enterNameTF = new JTextField();
	private JTextField enterAccountTF = new JTextField();
	private JPasswordField enterPasswordPWD = new JPasswordField();
	private JPasswordField resetPasswordPWD = new JPasswordField();

	// 创建字体大小
	private Font ft1 = new Font("仿宋", 1, 30);
	private Font ft2 = new Font("仿宋", 1, 20);

	public RegisterUI() {
		// 设置绝对布局
		this.setLayout(null);
		// 设置窗口标题
		this.setTitle("注册界面");
		// 设置窗口大小
		this.setSize(350, 380);
		// 设置窗口位置 居中
		this.setLocationRelativeTo(null);
		// 设置窗口关闭为程序结束
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		// 设置窗口不可改变大小
		this.setResizable(false);

		confirmButton.setBounds(20, 290, 120, 30);
		confirmButton.setFont(ft2);
		resetButton.setBounds(170, 290, 120, 30);
		resetButton.setFont(ft2);

		welcomeLabel.setBounds(20, 25, 200, 30);
		welcomeLabel.setFont(ft1);
		enterAccountLabel.setBounds(15, 70, 100, 50);
		enterAccountLabel.setFont(ft2);
		enterPasswordLabel.setBounds(15, 120, 100, 50);
		enterPasswordLabel.setFont(ft2);
		resetPasswordLabel.setBounds(15, 170, 100, 50);
		resetPasswordLabel.setFont(ft2);
		enterNameLabel.setBounds(15, 220, 100, 50);
		enterNameLabel.setFont(ft2);

		enterAccountTF.setBounds(110, 80, 180, 30);
		enterPasswordPWD.setBounds(110, 130, 180, 30);
		resetPasswordPWD.setBounds(110, 180, 180, 30);
		enterNameTF.setBounds(110, 230, 180, 30);

		this.add(confirmButton);
		this.add(resetButton);

		this.add(welcomeLabel);
		this.add(enterAccountLabel);
		this.add(enterPasswordLabel);
		this.add(resetPasswordLabel);
		this.add(enterNameLabel);

		this.add(enterAccountTF);
		this.add(enterPasswordPWD);
		this.add(resetPasswordPWD);
		this.add(enterNameTF);

	}

	public LoginUI getLogin() {
		return login;
	}

	public void setLogin(LoginUI login) {
		this.login = login;
	}

	public RegisterUI getRegister() {
		return register;
	}

	public void setRegister(RegisterUI register) {
		this.register = register;
	}

	public JButton getConfirmButton() {
		return confirmButton;
	}

	public void setConfirmButton(JButton confirmButton) {
		this.confirmButton = confirmButton;
	}

	public JButton getResetButton() {
		return resetButton;
	}

	public void setResetButton(JButton resetButton) {
		this.resetButton = resetButton;
	}

	public JTextField getEnterAccountTF() {
		return enterAccountTF;
	}

	public void setEnterAccountTF(JTextField enterAccountTF) {
		this.enterAccountTF = enterAccountTF;
	}

	public JPasswordField getEnterPasswordPWD() {
		return enterPasswordPWD;
	}

	public void setEnterPasswordPWD(JPasswordField enterPasswordPWD) {
		this.enterPasswordPWD = enterPasswordPWD;
	}

	public JPasswordField getResetPasswordPWD() {
		return resetPasswordPWD;
	}

	public void setResetPasswordPWD(JPasswordField resetPasswordPWD) {
		this.resetPasswordPWD = resetPasswordPWD;
	}

	public JTextField getEnterNameTF() {
		return enterNameTF;
	}

	public void setEnterNameTF(JTextField enterNameTF) {
		this.enterNameTF = enterNameTF;
	}

}
