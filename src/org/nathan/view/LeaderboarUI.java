package org.nathan.view;

/**
 * 排行榜界面
 */

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LeaderboarUI extends JFrame {

	private static final long serialVersionUID = -7057949298607211804L;
	// 创建按钮
	private JButton lv1Button = new JButton("难度1");
	private JButton lv2Button = new JButton("难度2");
	private JButton lv3Button = new JButton("难度3");

	// 创建标签
	private JLabel welcomLabel = new JLabel("排行榜");

	// 创建滚动条
	private JScrollPane pane = new JScrollPane();

	// 创建文本框
	private JTextArea tfd1 = new JTextArea("编号");

	// 创建字体大小
	private Font ft1 = new Font("", 1, 30);
	private Font ft2 = new Font("", 1, 20);

	public LeaderboarUI() {
		// 设置绝对布局
		this.setLayout(null);
		// 设置窗口标题名
		this.setTitle("排行榜");
		// 设置窗口大小
		this.setSize(550, 550);
		// 设置窗口位置居中
		this.setLocationRelativeTo(null);
		// 设置窗口关闭为结束
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		// 设置窗口不可改变大小
		this.setResizable(false);

		// 设置个个部件大小位置
		lv1Button.setBounds(20, 10, 100, 40);
		lv1Button.setFont(ft2);
		lv2Button.setBounds(160, 10, 100, 40);
		lv2Button.setFont(ft2);
		lv3Button.setBounds(300, 10, 100, 40);
		lv3Button.setFont(ft2);
		pane.setBounds(20, 120, 500, 350);
		tfd1.setFont(ft2);
		welcomLabel.setBounds(20, 60, 300, 50);
		welcomLabel.setFont(ft1);

		// 初始化获取所有图书展示
		tfd1.setText("");

		// 将文本域放进pane中
		pane.setViewportView(tfd1);

		this.add(lv1Button);
		this.add(lv2Button);
		this.add(lv3Button);
		this.add(pane);
		this.add(welcomLabel);

		lv1Button.setActionCommand("hard1");
		lv2Button.setActionCommand("hard2");
		lv3Button.setActionCommand("hard3");
	
	}

	public JButton getLv1Button() {
		return lv1Button;
	}

	public void setLv1Button(JButton lv1Button) {
		this.lv1Button = lv1Button;
	}

	public JButton getLv2Button() {
		return lv2Button;
	}

	public void setLv2Button(JButton lv2Button) {
		this.lv2Button = lv2Button;
	}

	public JButton getLv3Button() {
		return lv3Button;
	}

	public void setLv3Button(JButton lv3Button) {
		this.lv3Button = lv3Button;
	}

	public JScrollPane getPane() {
		return pane;
	}

	public void setPane(JScrollPane pane) {
		this.pane = pane;
	}

	public JTextArea getTfd1() {
		return tfd1;
	}

	public void setTfd1(JTextArea tfd1) {
		this.tfd1 = tfd1;
	}

	public JLabel getWelcomLabel() {
		return welcomLabel;
	}

	public void setWelcomLabel(JLabel welcomLabel) {
		this.welcomLabel = welcomLabel;
	}

}
