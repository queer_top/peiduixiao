package org.nathan.view;

/**
 * 登录界面
 */

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginUI extends JFrame {

	private static final long serialVersionUID = 5177583346656169343L;
	// 创建按钮
	private JButton loginButton = new JButton("登录");
	private JButton registerButton = new JButton("注册");

	// 创建标签
	private JLabel accountLabel = new JLabel("账号");
	private JLabel passwordLabel = new JLabel("密码");
	private JLabel welcomeLabel = new JLabel("欢迎登录");

	// 创建密码框
	private JTextField accountTF = new JTextField();
	private JPasswordField passwordPWF = new JPasswordField();

	// 创建字体大小
	private Font ft1 = new Font("仿宋", 1, 30);
	private Font ft2 = new Font("仿宋", 1, 20);

	public LoginUI() {
		// 设置绝对布局
		this.setLayout(null);
		// 设置窗口名称
		this.setTitle("登录界面");
		// 设置窗口大小
		this.setSize(360, 320);
		// 设置窗口位置 居中
		this.setLocationRelativeTo(null);
		// 设置窗口关闭为程序结束
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 设置窗口不可改变大小
		this.setResizable(false);

		registerButton.setBounds(40, 220, 100, 35);
		registerButton.setFont(ft2);
		loginButton.setBounds(180, 220, 100, 35);
		loginButton.setFont(ft2);

		welcomeLabel.setBounds(20, 30, 200, 50);
		welcomeLabel.setFont(ft1);
		accountLabel.setBounds(40, 90, 100, 50);
		accountLabel.setFont(ft2);
		passwordLabel.setBounds(40, 145, 100, 50);
		passwordLabel.setFont(ft2);

		accountTF.setBounds(100, 100, 180, 35);
		passwordPWF.setBounds(100, 150, 180, 35);

		// 调用
		this.add(registerButton);
		this.add(loginButton);
		this.add(welcomeLabel);
		this.add(accountLabel);
		this.add(passwordLabel);
		this.add(accountTF);
		this.add(passwordPWF);

	}

	public JButton getRegisterButton() {
		return registerButton;
	}

	public void setRegisterButton(JButton registerButton) {
		this.registerButton = registerButton;
	}

	public JButton getLoginButton() {
		return loginButton;
	}

	public void setLoginButton(JButton loginButton) {
		this.loginButton = loginButton;
	}

	public JLabel getAccountLabel() {
		return accountLabel;
	}

	public void setAccountLabel(JLabel accountLabel) {
		this.accountLabel = accountLabel;
	}

	public JTextField getAccountTF() {
		return accountTF;
	}

	public void setAccountTF(JTextField accountTF) {
		this.accountTF = accountTF;
	}

	public JPasswordField getPasswordPWF() {
		return passwordPWF;
	}

	public void setPasswordPWF(JPasswordField passwordPWF) {
		this.passwordPWF = passwordPWF;
	}

}
