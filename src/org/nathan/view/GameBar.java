package org.nathan.view;

/**
 * 菜单栏
 */

import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class GameBar extends JMenuBar {

	private static final long serialVersionUID = 1854414039108061866L;
	// 创建菜单
	private JMenu gameMenu = new JMenu("【游戏】");
	private JMenu helpMenu = new JMenu("【帮助】");
	private JMenu musicMenu = new JMenu("【音乐】");
	private JMenu difficultyMenu = new JMenu("【难度】");
	private JMenu leadBoardMenu = new JMenu("【排行榜】");

	// 创建菜单项
	private JMenuItem exitItem = new JMenuItem("退出游戏");
	private JMenuItem startItem = new JMenuItem("开始游戏");
	private JMenuItem restartItem = new JMenuItem("重新开始");

	private JCheckBox lv1CheckBox = new JCheckBox("难度一");
	private JCheckBox lv2CheckBox = new JCheckBox("难度二");
	private JCheckBox lv3CheckBox = new JCheckBox("难度三");

	private JCheckBox offCheckBox = new JCheckBox("音乐：关");
	private JCheckBox onCheckBox = new JCheckBox("音乐：开");

	private JMenuItem aboutItem = new JMenuItem("关于");

	private JMenuItem leadItem = new JMenuItem("排行榜");

	public GameBar() {

		// 游戏重新开始不可选
		restartItem.setEnabled(false);

		// 将菜单项放进菜单
		gameMenu.add(startItem);
		gameMenu.add(restartItem);
		gameMenu.add(exitItem);

		difficultyMenu.add(lv1CheckBox);
		difficultyMenu.add(lv2CheckBox);
		difficultyMenu.add(lv3CheckBox);

		musicMenu.add(onCheckBox);
		musicMenu.add(offCheckBox);

		helpMenu.add(aboutItem);

		leadBoardMenu.add(leadItem);

		// 将菜单放进菜单栏
		this.add(gameMenu);
		this.add(difficultyMenu);
		this.add(musicMenu);
		this.add(helpMenu);
		this.add(leadBoardMenu);

		// 动作监听
		exitItem.setActionCommand("exit");
		startItem.setActionCommand("start");
		restartItem.setActionCommand("restart");

		lv1CheckBox.setActionCommand("lv1");
		lv2CheckBox.setActionCommand("lv2");
		lv3CheckBox.setActionCommand("lv3");

		onCheckBox.setActionCommand("on");
		offCheckBox.setActionCommand("off");

		aboutItem.setActionCommand("about");

		leadItem.setActionCommand("lead");
	}

	// get set方法
	public JMenu getGameMenu() {
		return gameMenu;
	}

	public void setGameMenu(JMenu gameMenu) {
		this.gameMenu = gameMenu;
	}

	public JMenu getDifficultyMenu() {
		return difficultyMenu;
	}

	public void setDifficultyMenu(JMenu difficultyMenu) {
		this.difficultyMenu = difficultyMenu;
	}

	public JMenu getMusicMenu() {
		return musicMenu;
	}

	public void setMusicMenu(JMenu musicMenu) {
		this.musicMenu = musicMenu;
	}

	public JMenu getHelpMenu() {
		return helpMenu;
	}

	public void setHelpMenu(JMenu helpMenu) {
		this.helpMenu = helpMenu;
	}

	public JMenuItem getStartItem() {
		return startItem;
	}

	public void setStartItem(JMenuItem startItem) {
		this.startItem = startItem;
	}

	public JMenuItem getRestartItem() {
		return restartItem;
	}

	public void setRestartItem(JMenuItem restartItem) {
		this.restartItem = restartItem;
	}

	public JMenuItem getExitItem() {
		return exitItem;
	}

	public void setExitItem(JMenuItem exitItem) {
		this.exitItem = exitItem;
	}

	public JCheckBox getLv1CheckBox() {
		return lv1CheckBox;
	}

	public void setLv1CheckBox(JCheckBox lv1CheckBox) {
		this.lv1CheckBox = lv1CheckBox;
	}

	public JCheckBox getLv2CheckBox() {
		return lv2CheckBox;
	}

	public void setLv2CheckBox(JCheckBox lv2CheckBox) {
		this.lv2CheckBox = lv2CheckBox;
	}

	public JCheckBox getLv3CheckBox() {
		return lv3CheckBox;
	}

	public void setLv3CheckBox(JCheckBox lv3CheckBox) {
		this.lv3CheckBox = lv3CheckBox;
	}

	public JCheckBox getOnCheckBox() {
		return onCheckBox;
	}

	public void setOnCheckBox(JCheckBox onCheckBox) {
		this.onCheckBox = onCheckBox;
	}

	public JCheckBox getOffCheckBox() {
		return offCheckBox;
	}

	public void setOffCheckBox(JCheckBox offCheckBox) {
		this.offCheckBox = offCheckBox;
	}

	public JMenuItem getAboutItem() {
		return aboutItem;
	}

	public void setAboutItem(JMenuItem aboutItem) {
		this.aboutItem = aboutItem;
	}

	public JMenu getLeadBoardMenu() {
		return leadBoardMenu;
	}

	public void setLeadBoardMenu(JMenu leadBoardMenu) {
		this.leadBoardMenu = leadBoardMenu;
	}

	public JMenuItem getLeadItem() {
		return leadItem;
	}

	public void setLeadItem(JMenuItem leadItem) {
		this.leadItem = leadItem;
	}

}
