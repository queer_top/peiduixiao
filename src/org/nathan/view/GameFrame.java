package org.nathan.view;

/**
 * 主容器
 */

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GameFrame extends JFrame implements WindowListener {

	private static final long serialVersionUID = -4480305725744311516L;
	private GameBar bar = new GameBar();
	private GamePanel gamePanel = new GamePanel();

	public GameFrame() {
		this.setJMenuBar(bar);
		this.setTitle("配对消");
		this.setSize(620, 740);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setResizable(false);

		// 替换默认面板添加自定义面板
		this.setContentPane(gamePanel);

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowClosing(WindowEvent e) {
		int result = JOptionPane.showConfirmDialog(gamePanel, "您确定退出游戏吗？");
		if (result == 0) {
			System.exit(result);
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	// get set方法
	public GameBar getBar() {
		return bar;
	}

	public void setBar(GameBar bar) {
		this.bar = bar;
	}

	public GamePanel getGamePanel() {
		return gamePanel;
	}

	public void setGamePanel(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

}
