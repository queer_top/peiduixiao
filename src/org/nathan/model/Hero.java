package org.nathan.model;

/**
 * 英雄
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

public class Hero {

	private int id;
	private int width = Database.size;
	private int height = Database.size;
	private int startX;
	private int startY;
	private int check = 0;
	private Image img;

	private Font font = new Font("", Font.BOLD, 20);

	public Hero(int id, int width, int height, int startX, int startY, int check, Image img) {
		super();
		this.id = id;
		this.width = width;
		this.height = height;
		this.startX = startX;
		this.startY = startY;
		this.check = check;
		this.img = img;
	}

	public Hero(int num, Image images) {
		this.id = num;
		this.img = images;
	}

	// 画英雄头像的方法
	public void showMe(Graphics g, int lattice) {
		g.drawImage(img, startX, startY, 576 / lattice, 576 / lattice, null);
		g.setFont(font);
		g.setColor(Color.WHITE);
		g.drawString(id + "", startX + 5, startY + 20);
		
		if(check == 0) {
			g.setColor(Color.BLUE);
			g.drawRect(startX, startY,  576 / lattice, 576 / lattice);
		}
		if(check == 1){
			g.setColor(Color.WHITE);
			g.drawRect(startX + 5, startY + 5,  576 / lattice - 10, 576 / lattice - 10);
		}
	}

	public Hero(int id, int width, int height, int startX, int startY, Image img, Font font) {
		super();
		this.id = id;
		this.width = width;
		this.height = height;
		this.startX = startX;
		this.startY = startY;
		this.img = img;
		this.font = font;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	public int getCheck() {
		return check;
	}

	public void setCheck(int check) {
		this.check = check;
	}

}
