package org.nathan.model;

/**
 * 共享参数库
 */

import java.awt.Image;
import javax.swing.ImageIcon;

public class Database {

	// 网格数
	public static int lattice = 4;

	// 实心矩形的XY
	public static final int X_GAP = 20;
	public static final int Y_GAP = 20;

	// 获取玩家名字和id
	public static String name;
	public static int id;

	// 难度状态
	public static int state = 1;

	// 初始图片大小
	public static int size = 576 / lattice;

	// 游戏状态
	public static int status = 0; // 0:未开始 1:加载中 2:游戏中

	// 计时
	public static int time = 0;

	// 消除图片
	public static Image images = new ImageIcon("image/remove.gif").getImage();

}
