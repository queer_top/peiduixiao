package org.nathan.controller;

/**
 * 初始化监听
 */

import org.nathan.view.GameFrame;
import org.nathan.view.LeaderboarUI;
import org.nathan.view.LoginUI;
import org.nathan.view.RegisterUI;

public class Application {

	public static void run() {

		// 创建窗口
		LoginUI log = new LoginUI();
		RegisterUI reg = new RegisterUI();
		GameFrame gameFrame = new GameFrame();
		LeaderboarUI leaderboarUI = new LeaderboarUI();
		GameBarLis gameBarLis = new GameBarLis(gameFrame, leaderboarUI);

		// 创建监听
		FrontLis frontLis = new FrontLis(gameFrame);
		ResetButtonLis resetButtonLis = new ResetButtonLis(reg);
		RegisterUILis registerUILis = new RegisterUILis(log, reg);
		LeadBoardLis leadBoardLis = new LeadBoardLis(leaderboarUI);
		LeaderBoardLis leaderBoardLis = new LeaderBoardLis(leaderboarUI);
		ConfirmButtonLis confirmButtonLis = new ConfirmButtonLis(log, reg);
		LoginButtonLis loginButtonLis = new LoginButtonLis(log, gameFrame);
		RegisterButtonLis registerButtonLis = new RegisterButtonLis(log, reg);

		// 安装监听

		gameFrame.getGamePanel().addMouseListener(frontLis);

		log.getLoginButton().addMouseListener(loginButtonLis);
		log.getRegisterButton().addMouseListener(registerButtonLis);

		reg.getResetButton().addMouseListener(resetButtonLis);
		reg.getConfirmButton().addMouseListener(confirmButtonLis);

		gameFrame.getBar().getLeadItem().addActionListener(gameBarLis);
		gameFrame.getBar().getExitItem().addActionListener(gameBarLis);
		gameFrame.getBar().getStartItem().addActionListener(gameBarLis);
		gameFrame.getBar().getAboutItem().addActionListener(gameBarLis);
		gameFrame.getBar().getOnCheckBox().addActionListener(gameBarLis);
		gameFrame.getBar().getRestartItem().addActionListener(gameBarLis);
		gameFrame.getBar().getLv1CheckBox().addActionListener(gameBarLis);
		gameFrame.getBar().getLv2CheckBox().addActionListener(gameBarLis);
		gameFrame.getBar().getLv3CheckBox().addActionListener(gameBarLis);
		gameFrame.getBar().getOffCheckBox().addActionListener(gameBarLis);

		leaderboarUI.getLv1Button().addActionListener(leaderBoardLis);
		leaderboarUI.getLv2Button().addActionListener(leaderBoardLis);
		leaderboarUI.getLv3Button().addActionListener(leaderBoardLis);

		reg.addWindowListener(registerUILis);
		gameFrame.addWindowListener(gameFrame);
		leaderboarUI.addWindowListener(leadBoardLis);

		// 设置登入窗口显示
		log.setVisible(true);
	}
}
