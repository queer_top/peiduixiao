package org.nathan.controller;

/**
 * 排行榜窗口监听
 */

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import org.nathan.view.LeaderboarUI;

public class LeadBoardLis implements WindowListener {

	private LeaderboarUI leaderboa;

	public LeadBoardLis(LeaderboarUI leaderboa) {
		super();
		this.leaderboa = leaderboa;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void windowClosing(WindowEvent e) {
		leaderboa.setVisible(false);

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO 自动生成的方法存根

	}

}
