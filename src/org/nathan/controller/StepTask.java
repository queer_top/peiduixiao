package org.nathan.controller;

/**
 * 动画计时器
 */

import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.nathan.model.Database;
import org.nathan.model.Hero;
import org.nathan.view.GameFrame;

public class StepTask extends TimerTask {

	private GameFrame gameFrame;

	public StepTask(GameFrame gameFrame) {
		super();
		this.gameFrame = gameFrame;
	}

	@Override
	public void run() {

		// 图片随机出现
		Random ran = new Random();
		// 前台集合
		ArrayList<Hero> frontList = gameFrame.getGamePanel().getFrontList();

		// 后台集合
		ArrayList<Hero> backList = gameFrame.getGamePanel().getHerosList();

		// 将后台集合随机添加到前台集合
		if (backList.size() > 0) {
			int index = ran.nextInt(backList.size());
			frontList.add(backList.remove(index));
		} else {
			JOptionPane.showMessageDialog(gameFrame, "游戏加载完成！");
			Database.status = 2;
			this.cancel();
		}
	}

}
