package org.nathan.controller;

/**
 * 排行榜按钮的监听
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.nathan.tool.SetTextTool;
import org.nathan.view.LeaderboarUI;

public class LeaderBoardLis implements ActionListener{

	private LeaderboarUI leaderboar;
	
	public LeaderBoardLis(LeaderboarUI leaderboar) {
		super();
		this.leaderboar = leaderboar;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		String action = e.getActionCommand();
		
		if(action.equals("hard1")) {
			leaderboar.getWelcomLabel().setText("难度【一】排名");
			leaderboar.getTfd1().setText(SetTextTool.setText(1));
			
		}else if(action.equals("hard2")) {
			leaderboar.getWelcomLabel().setText("难度【二】排名");
			leaderboar.getTfd1().setText(SetTextTool.setText(2));
			
		}else {
			leaderboar.getWelcomLabel().setText("难度【三】排名");
			leaderboar.getTfd1().setText(SetTextTool.setText(3));
		}
		
		
	}

}
