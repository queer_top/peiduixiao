package org.nathan.controller;

/**
 * 注册界面监听
 */

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import org.nathan.view.LoginUI;
import org.nathan.view.RegisterUI;

public class RegisterUILis implements WindowListener {

	private LoginUI login;
	private RegisterUI register;

	public RegisterUILis(LoginUI login, RegisterUI register) {
		super();
		this.login = login;
		this.register = register;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowClosing(WindowEvent e) {
		login.setVisible(true);
		register.setVisible(false);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO 自动生成的方法存根
	}

}
