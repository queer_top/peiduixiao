package org.nathan.controller;

/**
 * 消除动画计时器
 */

import java.util.ArrayList;
import java.util.TimerTask;
import org.nathan.model.Hero;


public class EliminateTask extends TimerTask {

	private ArrayList<Hero> frontList;
	private Hero hero1;
	private Hero hero2;

	public EliminateTask(ArrayList<Hero> frontList, Hero hero1, Hero hero2) {
		super();
		this.frontList = frontList;
		this.hero1 = hero1;
		this.hero2 = hero2;
	}

	@Override
	public void run() {
		
		// 删除前台集合
		frontList.remove(hero1);
		frontList.remove(hero2);
	}
}
