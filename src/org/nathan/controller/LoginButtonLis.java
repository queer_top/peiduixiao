package org.nathan.controller;

/**
 * 登入按钮监听
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import org.nathan.tool.CheckLoginTool;

import org.nathan.view.GameFrame;
import org.nathan.view.LoginUI;

public class LoginButtonLis implements MouseListener {

	private LoginUI login;
	private GameFrame gameFrame;

	public LoginButtonLis(LoginUI login, GameFrame gameFrame) {
		super();
		this.login = login;
		this.gameFrame = gameFrame;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		// 获取文本框内容
		String acc = login.getAccountTF().getText().trim();
		String pwd = new String(login.getPasswordPWF().getPassword()).trim();

		// 非空判断
		if (acc.equals("") || pwd.equals("")) {
			JOptionPane.showMessageDialog(login, "请填写完整！！！");
			return;
		}

		// 数据库判断
		boolean flag = CheckLoginTool.checkLogin(acc, pwd);

		// 清空登入文本框，游戏界面展示
		if (flag == true) {
			login.getAccountTF().setText("");
			login.getPasswordPWF().setText("");
			login.setVisible(false);
			gameFrame.setVisible(true);
		} else {
			return;
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

}
