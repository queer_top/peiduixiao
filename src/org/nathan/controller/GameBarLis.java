package org.nathan.controller;

/**
 * 菜单栏监听
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Timer;

import javax.swing.JOptionPane;

import org.nathan.model.Database;
import org.nathan.model.Hero;
import org.nathan.tool.MusicTool;
import org.nathan.tool.TimeTesk;
import org.nathan.tool.Tool;
import org.nathan.view.GameBar;
import org.nathan.view.GameFrame;
import org.nathan.view.LeaderboarUI;

public class GameBarLis implements ActionListener {

	private GameFrame gameFrame;
	private LeaderboarUI leaderboar;
	private ArrayList<Hero> heros;
	private MusicTool musicTool = new MusicTool();
	private Timer stepTaskTimer = new Timer();
	private Timer timeTesk = new Timer();

	public GameBarLis(GameFrame gameFrame, LeaderboarUI leaderboar) {
		super();
		this.gameFrame = gameFrame;
		this.leaderboar = leaderboar;
		heros = gameFrame.getGamePanel().getHerosList();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String action = e.getActionCommand();
		GameBar gameBar = gameFrame.getBar();
		
		if (action.equals("start")) {

			musicTool.getBgmAc().loop();

			JOptionPane.showMessageDialog(gameBar, "开始加载");

			// 游戏状态为加载中
			Database.status = 1;

			// 清空计时器
			Database.time = 0;

			// 游戏初始化
			Tool.initheros(heros, Database.lattice);
			Tool.updateCoordinates(heros, Database.lattice);

			// 动画
			stepTaskTimer.schedule(new StepTask(gameFrame), 100, 100);

			// 设置重新开始可用，开始游戏不可用
			gameBar.getStartItem().setEnabled(false);
			gameBar.getRestartItem().setEnabled(true);

			// 计时器
			timeTesk.schedule(new TimeTesk(gameFrame), 1000, 1000);

		} else if (action.equals("restart")) {

			// 游戏为加载中
			Database.status = 1;

			// 取消动画和计时的上一定时器，新建定时器
			stepTaskTimer.cancel();
			stepTaskTimer = new Timer();
			timeTesk.cancel();
			timeTesk = new Timer();

			// 清空计时器
			Database.time = 0;

			JOptionPane.showMessageDialog(gameBar, "开始加载");

			// 游戏状态为加载中
			Database.status = 1;

			// 设置重新开始可用，开始游戏不可用
			gameBar.getStartItem().setEnabled(false);
			gameBar.getRestartItem().setEnabled(true);

			// 清空前台集合
			gameFrame.getGamePanel().getFrontList().clear();

			// 游戏初始化
			Tool.initheros(heros, Database.lattice);
			Tool.updateCoordinates(heros, Database.lattice);

			// 动画
			stepTaskTimer.schedule(new StepTask(gameFrame), 100, 100);

			// 计时器
			timeTesk.schedule(new TimeTesk(gameFrame), 1000, 1000);

		} else if (action.equals("exit")) {
			int result = JOptionPane.showConfirmDialog(null, "您真的要退出吗？？？");
			if (result == 0) {
				System.exit(0);
			}

		} else if (action.equals("lv1")) {

			// 清空计时器
			Database.time = 0;
			timeTesk.cancel();
			timeTesk = new Timer();

			// 设置重新开始不可用，开始游戏可用
			gameBar.getRestartItem().setEnabled(false);
			gameBar.getStartItem().setEnabled(true);

			// 前台集合清空
			gameFrame.getGamePanel().getFrontList().clear();
			gameFrame.getGamePanel().getHerosList().clear();

			// 音乐关复选框清空
			gameBar.getOffCheckBox().setSelected(false);

			// 取消其他难度复选框
			gameBar.getLv2CheckBox().setSelected(false);
			gameBar.getLv3CheckBox().setSelected(false);

			// 设置状态
			Database.state = 1;
			Database.lattice = 4;

		} else if (action.equals("lv2")) {

			Database.time = 0;
			timeTesk.cancel();
			timeTesk = new Timer();

			// 设置重新开始不可用，开始游戏可用
			gameBar.getRestartItem().setEnabled(false);
			gameBar.getStartItem().setEnabled(true);

			// 前台集合清空
			gameFrame.getGamePanel().getFrontList().clear();
			gameFrame.getGamePanel().getHerosList().clear();

			// 音乐关复选框清空
			gameBar.getOffCheckBox().setSelected(false);

			// 取消其他难度复选框
			gameBar.getLv1CheckBox().setSelected(false);
			gameBar.getLv3CheckBox().setSelected(false);

			// 设置状态
			Database.state = 2;
			Database.lattice = 6;

		} else if (action.equals("lv3")) {

			// 清空计时器
			Database.time = 0;
			timeTesk.cancel();
			timeTesk = new Timer();

			// 设置重新开始不可用，开始游戏可用
			gameBar.getRestartItem().setEnabled(false);
			gameBar.getStartItem().setEnabled(true);

			// 前台集合清空
			gameFrame.getGamePanel().getFrontList().clear();
			gameFrame.getGamePanel().getHerosList().clear();

			// 音乐关复选框清空
			gameBar.getOffCheckBox().setSelected(false);

			// 取消其他难度复选框
			gameBar.getLv1CheckBox().setSelected(false);
			gameBar.getLv2CheckBox().setSelected(false);

			// 设置状态
			Database.state = 3;
			Database.lattice = 8;

		} else if (action.equals("on")) {

			musicTool.getBgmAc().loop();
			// 取消音乐关复选框
			gameBar.getOffCheckBox().setSelected(false);

		} else if (action.equals("off")) {

			// 关闭关闭
			musicTool.getBgmAc().stop();

			// 取消音乐开复选框
			gameBar.getOnCheckBox().setSelected(false);

		} else if (action.equals("about")) {

			// 关于内容弹窗
			JOptionPane.showMessageDialog(null,
					"Author：Nathan\r\nCompany：Liu Corporation\r\nVersion：V1.0\r\nTel：123456789");

		} else if (action.equals("lead")) {
			leaderboar.setVisible(true);
		}
	}

}
