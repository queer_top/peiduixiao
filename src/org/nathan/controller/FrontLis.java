package org.nathan.controller;

/**
 * 点击前台集合监听
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Timer;
import org.nathan.model.Database;
import org.nathan.model.Hero;
import org.nathan.tool.Tool;
import org.nathan.view.GameFrame;

public class FrontLis implements MouseListener {

	private GameFrame gameFrame;
	private ArrayList<Hero> comparisonList;
	private Timer eliminateTask = new Timer();

	public FrontLis(GameFrame gameFrame) {
		super();
		this.gameFrame = gameFrame;
		comparisonList = gameFrame.getGamePanel().getComparisonList();
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		// 游戏状态判断
		if (Database.status != 2) {
			return;
		}

		// 鼠标点击判断
		Hero hero = Tool.mouseCleck(e.getX(), e.getY(), gameFrame.getGamePanel().getFrontList(), Database.lattice);

		if (hero != null) {
			hero.setCheck(1);
			comparisonList.add(hero);
		}
		// 判断集合添加
		if (comparisonList.size() == 2) {
			hero.setCheck(0);
			if (comparisonList.get(0) == null || comparisonList.get(1) == null) {
				comparisonList.clear();
				return;
			}
			
			Hero hero1 = comparisonList.get(0);
			Hero hero2 = comparisonList.get(1);

			if (hero1 == hero2) {
				hero.setCheck(0);
				comparisonList.clear();
				return;
				
			} else {
				hero1.setCheck(0);
				hero2.setCheck(0);
				if (hero1.getId() == hero2.getId()) {

					// 消除动画
					hero1.setImg(Database.images);
					hero2.setImg(Database.images);

					eliminateTask.schedule(new EliminateTask(gameFrame.getGamePanel().getFrontList(), hero1, hero2),
							1000, 1000);

				}
			}
			comparisonList.clear();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO 自动生成的方法存根

	}

}
