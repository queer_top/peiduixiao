package org.nathan.controller;

/**
 * 确认注册按钮监听
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import org.nathan.tool.CheckRegisTool;
import org.nathan.view.LoginUI;
import org.nathan.view.RegisterUI;

public class ConfirmButtonLis implements MouseListener {

	private LoginUI login;
	private RegisterUI register;

	public ConfirmButtonLis(LoginUI login, RegisterUI register) {
		super();
		this.login = login;
		this.register = register;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		// 获取文本框内容
		String acc = register.getEnterAccountTF().getText().trim();
		String pwd = new String(register.getEnterPasswordPWD().getPassword()).trim();
		String reset = new String(register.getResetPasswordPWD().getPassword()).trim();
		String name = register.getEnterNameTF().getText().trim();

		// 非空判断
		if (acc.equals("") || pwd.equals("") || reset.equals("") || name.equals("")) {
			JOptionPane.showMessageDialog(register, "请填写完整");
			return;
		}

		// 两次是否密码一致
		if (!pwd.equals(reset)) {
			JOptionPane.showMessageDialog(register, "两次密码不一致!!!");
			return;
		}

		// 数据库判断
		CheckRegisTool.checkRegis(acc, pwd, reset, name);

		// 注册界面关闭，登入界面打开
		register.setVisible(false);
		login.setVisible(true);

		// 清空注册文本框内容
		register.getEnterNameTF().setText("");
		register.getEnterAccountTF().setText("");
		register.getEnterPasswordPWD().setText("");
		register.getResetPasswordPWD().setText("");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

}
