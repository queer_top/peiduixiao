package org.nathan.controller;

/**
 * 重置按钮监听
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.nathan.view.RegisterUI;

public class ResetButtonLis implements MouseListener {

	private RegisterUI reg;

	public ResetButtonLis(RegisterUI reg) {
		super();
		this.reg = reg;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// 清空注册文本框内容
		reg.getEnterNameTF().setText("");
		reg.getEnterAccountTF().setText("");
		reg.getResetPasswordPWD().setText("");
		reg.getEnterPasswordPWD().setText("");

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

}
