package org.nathan.controller;

/**
 * 注册按钮监听
 */

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.nathan.view.LoginUI;
import org.nathan.view.RegisterUI;

public class RegisterButtonLis implements MouseListener {

	private LoginUI login;
	private RegisterUI register;

	public RegisterButtonLis(LoginUI login, RegisterUI register) {
		super();
		this.login = login;
		this.register = register;

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// 登入界面关闭 ，注册界面打开
		register.setVisible(true);
		login.setVisible(false);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO 自动生成的方法存根
	}

}
